
package com.example.entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Product {
	@Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "product_id")
	 private Long id;
	 @ManyToOne
	 @JoinColumn(name = "product_type")
	 private productCategory productCategory;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	@Column(name = "product_desc")
	@NotNull
  private String desc;
	
	/* @Column(name = "product_type")
	 @NotNull
 private String type;
	*/
	 @Column(name = "product_inventory")
	 @NotNull
 private int inventory;
	
 @Column(name = "product_safeinventory")
 @NotNull
 private int safeinventory;
public String getDescribe() {
	return desc;
}
public void setDescribe(String desc) {
	this.desc = desc;
}
public productCategory getProductCategory() {
	return productCategory;
}

public void setProductCategory(productCategory productCategory) {
	this.productCategory = productCategory;
}
public int getInventory() {
	return inventory;
}
public void setInventory(int inventory) {
	this.inventory = inventory;
}
public int getSafeinventory() {
	return safeinventory;
}
public void setSafeinventory(int safeinventory) {
	this.safeinventory = safeinventory;
}
@Column(name = "product_brand")
@NotNull
private String brand;
public String getBrand() {
	return brand;
}
public void setBrand(String brand) {
	this.brand = brand;
}
@Column(name = "product_createdate")
@NotNull
private String createdate;
public String getCreatedate() {
	return createdate;
}
public void setcreatedate(String createdate) {
	this.createdate = createdate;
}
@Column(name = "product_vendor")
@NotNull
private String vendor;
public String getVendor() {
	return vendor;
}
public void setvendor(String vendor) {
	this.vendor = vendor;
}

@Column(name = "product_price")
@NotNull
private String price;
public String getPrice() {
	return price;
}
public void setprice(String price) {
	this.price = price;
}
}