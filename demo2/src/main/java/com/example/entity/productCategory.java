package com.example.entity;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="product_category")
public class productCategory {
	@Id
	@Column(name = "product_code")
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 private Long id;
	 @Column(name = "product_class")
	 private String type;
	 
	@OneToMany(mappedBy = "productCategory")
	 private List<Product> product;
	 
	
	 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	  public Iterable<Product> getproducts() {
		  return product;
		 }
	
}
