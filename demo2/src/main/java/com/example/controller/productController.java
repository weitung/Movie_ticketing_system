package com.example.controller;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.entity.Product;
import com.example.entity.productCategory;

import java.sql.SQLException;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.dao.productCategoryDAO;
import com.example.dao.productDAO;
import com.example.entity.Product;
@Controller
public class productController {
	@Autowired
	 productDAO dao;
	 @Autowired
	 productCategoryDAO categoryDao;
    @RequestMapping(value = "/productCreate", method = RequestMethod.GET)
    public ModelAndView openForm(Product p) {
    	ModelAndView model = new ModelAndView("productCreate");
    	 Iterable<productCategory> categories = categoryDao.findAll();
    	  model.addObject("allproductCategories", categories);
       return model;
    }
    @RequestMapping(value = { "/productRetrieveByCategory" }, method = RequestMethod.POST)
    public ModelAndView retrieveproductsByCategory(
      @RequestParam(value = "productCategory", required = false, defaultValue = "1") Long productCategory) {
     ModelAndView model = new ModelAndView("productlist");
     
     Iterable<productCategory> categories = categoryDao.findAll();
     model.addObject("allproductCategories", categories);
     productCategory category = categoryDao.findOne(productCategory);
     model.addObject("productCategory", category);
     
     model.addObject("products", category.getproducts());
     return model;
    }

    @RequestMapping(value = "/productCreate", method = RequestMethod.POST)
    public ModelAndView processFormCreate(@Valid@ModelAttribute Product pro, BindingResult bindingResult) throws SQLException {
       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
       if (bindingResult.hasErrors()) {
           model = new ModelAndView("productCreate");
           return model;
         }
       dao.save(pro);
              
       //model.addObject(pro);
       return model;
    }
    
    @RequestMapping(value = {"/productRetrieveAll","/"}, method = RequestMethod.GET)
    public ModelAndView retrieveCustomers() throws SQLException{
    
    Iterable<Product> products = dao.findAll();
       ModelAndView model = new ModelAndView("productList");
       
       Iterable<productCategory> categories = categoryDao.findAll();
       model.addObject("allproductCategories", categories);
       productCategory category = categories.iterator().next();//get first category
       model.addObject("productCategory", category);
       
       model.addObject("products",products);
       return model;
    }

 @RequestMapping(value = "/productUpdate", method = RequestMethod.GET)
    public ModelAndView openFormUpdate(@RequestParam(value="id", required=false, defaultValue="1") long id ) {
       ModelAndView model = new ModelAndView("prodctUpdate");
       Product product = dao.findOne(id);
       model.addObject(product);
       Iterable<productCategory> categories = categoryDao.findAll();
       model.addObject("allproductCategories", categories);
       return model;
    }

    @RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
    public ModelAndView processFormUpdate(@Valid@ModelAttribute Product pro, BindingResult bindingResult) throws SQLException {
       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
       /*if (bindingResult.hasErrors()) {
           model = new ModelAndView("prodctUpdate");
           return model;
         }*/
       dao.save(pro);             
       return model;
    }
    
 @RequestMapping(value = "/productDelete", method = RequestMethod.GET)
    public ModelAndView deleteCustomer(@RequestParam(value="id", required=false, defaultValue="1") Long id) {
       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
       dao.delete(id);
       return model;
    }
 
}
