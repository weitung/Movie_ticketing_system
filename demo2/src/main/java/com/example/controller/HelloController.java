package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class HelloController {

    @RequestMapping("/hello")
    public ModelAndView home(@ModelAttribute("name") String name) {
    	
    	ModelAndView model = new ModelAndView("hello");
    	ModelAndView model2 = new ModelAndView("productCreate");
    	model.addObject("name", name);
       if(name.equals("sa")){
    	   return model2;
       }else{
       return model;}
    }
    @RequestMapping("/helloWithId")
    public ModelAndView home() {
       ModelAndView model = new ModelAndView("hellowithid");
       
       return model;
    }

}