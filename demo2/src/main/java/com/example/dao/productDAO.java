package com.example.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Product;
import com.example.entity.productCategory;

public interface productDAO extends CrudRepository<Product, Long>{

	public Iterable<Product> findByproductCategory(productCategory category);
	 }