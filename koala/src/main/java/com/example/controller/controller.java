package com.example.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import com.example.DAO.MovieDAO;
import com.example.DAO.memberDAO;
import com.example.entity.Member;
import com.example.entity.Movie;
import com.example.entity.screening;
import com.example.entity.seat;





@Controller
public class controller {
	
	@Autowired
	MovieDAO dao;
	@Autowired
	memberDAO memdao;

	@RequestMapping(value = "/becomemember", method = RequestMethod.GET)
	public ModelAndView openFormCreate() {
	   ModelAndView model = new ModelAndView("becomemember");
	   return model;
	}
	@RequestMapping(value = "/becomemember", method = RequestMethod.POST)
	public ModelAndView processFormCreate(@ModelAttribute Member mem) throws SQLException {
	   ModelAndView model = new ModelAndView("successmember");
	   mem.setId(memdao.findid()+1);
	   memdao.insert(mem);           
	   model.addObject(mem);
	   return model;
		 
	  
	}
	@RequestMapping(value = "/memberlogin", method = RequestMethod.GET)
	public ModelAndView openFormCreate1() {
	   ModelAndView model = new ModelAndView("memberlogin");
	   return model;
	}
	
	@RequestMapping(value = {"/home", "/"})
    public ModelAndView homepage() {
    	ModelAndView model = new ModelAndView("homepage");
    	 
       return model;
    }
	@RequestMapping(value = {"/movielist"})
    public ModelAndView movielist() throws SQLException{
    	ModelAndView model = new ModelAndView("movielist");
    	 List<Movie> mov=dao.findall();
    	 model.addObject("movlist", mov);
       return model;
    }
	@RequestMapping(value = {"/booking"})
    public ModelAndView moviebooked() throws SQLException{
    	ModelAndView model = new ModelAndView("booking");
    	
    	 List<Movie> mov=dao.findall();
    	 model.addObject("movlist", mov);
       return model;
    }
	@RequestMapping(value = "/booking1",method=RequestMethod.GET)
    public ModelAndView bookform(@ModelAttribute("moviechoosed") int movieid) throws SQLException{
    	ModelAndView model = new ModelAndView("booking");
    	
    	 List<screening> movch=dao.findscreeningbymovieid(movieid);
    	 
    	 model.addObject("scr", movch);
    	 List<Movie> mov=dao.findall();
    	 model.addObject("movlist", mov);
    	 model.addObject("movieid",movieid);
       return model;
    }
	@RequestMapping(value = "/booking2",method=RequestMethod.POST)
    public ModelAndView bookform2(@ModelAttribute("moviechoosed2") int screeningid,HttpSession session) throws SQLException{
    	ModelAndView model = new ModelAndView("booking2fix");
    	int seatquantity=dao.findseatquqntitybyscreningid(screeningid);
    	screening scr=dao.findscreeningbyscreningid(screeningid).get(0);
    	session.setAttribute("screeningid", screeningid);
    	model.addObject("scrlist", scr);
    	model.addObject("seatquantity",seatquantity);
       return model;
    }
	@RequestMapping(value = "/seatchoosed")
    public ModelAndView seatchoosed(@ModelAttribute("number") int quantity,HttpSession session) throws SQLException{
    	ModelAndView model = new ModelAndView("seatchoosed");
    	session.setAttribute("number", quantity);
    	int screeningid=(int)session.getAttribute("screeningid");
    	String room=dao.findroombyscreningid(screeningid);
    	List<seat> seatlist=dao.findseatbyscreningid(screeningid);
    	model.addObject("seatlist",seatlist);
    	model.addObject("number",quantity);
    	if(room.equalsIgnoreCase("a")){
    		model.addObject("picture","rooma.PNG");
    	}
    	else{
    		model.addObject("picture","roomb.PNG");
    	}
       return model;
    }
	@RequestMapping(value = "/bookingdone")
	public ModelAndView bookingdone(@RequestParam(value = "seatchoose") int[] seatid, HttpSession session) throws SQLException{
		/*int[] test=new int[(int)session.getAttribute("number")];
		for(int i=0;i<(int)session.getAttribute("number")-1;i++){
			test[i]=Integer.parseInt(seatid[i]);
		};*/
		ModelAndView model = new ModelAndView("bookingdone");
		int screeningid=(int)session.getAttribute("screeningid");
		for(int i=0;i<(int)session.getAttribute("number");i++){
		dao.seatupdate(seatid[i], screeningid);}
		return model;
	}
	@RequestMapping(value = {"/admin"})
    public ModelAndView admin() {
    	ModelAndView model = new ModelAndView("admin");
       return model;
    }
	@RequestMapping(value = {"/setmovies"})
    public ModelAndView setmovie() {
    	ModelAndView model = new ModelAndView("setmovie");
    	List<Movie> mov=dao.findall();
   	 model.addObject("movlist", mov);
       return model;
    }
	@RequestMapping(value = {"/setscreening"})
    public ModelAndView setscreening() {
    	ModelAndView model = new ModelAndView("setscreening");
    	List<Movie> mov=dao.findall();
      	 model.addObject("movlist", mov);
      	List<screening> scrlist=dao.findallscreening();
    	model.addObject("scrlist",scrlist);
       return model;
    }
	@RequestMapping(value = {"/deletemovie"})
    public ModelAndView deletemovie(@ModelAttribute("movieid") int id) {
    	ModelAndView model = new ModelAndView("setmovie");
    	dao.deletemovie(id);
    	List<Movie> mov=dao.findall();
     	 model.addObject("movlist", mov);
       return model;
    }
	@RequestMapping(value = {"/updatemovie"})
    public ModelAndView createmovie(@ModelAttribute("movieid") int id,HttpSession session) {
		session.setAttribute("movieid", id);
    	ModelAndView model = new ModelAndView("update");
    	Movie mov=dao.findmoviebymovieid(id).get(0);
    	model.addObject("mov",mov);
       return model;
    }
	@RequestMapping(value = {"/createscreening"})
    public ModelAndView updatescr(@ModelAttribute("movieid") int id,HttpSession session) {
		session.setAttribute("movieid", id);
    	ModelAndView model = new ModelAndView("creatscreening");
    	List<screening> scrlist=dao.findallscreening();
    	model.addObject(scrlist);
       return model;
    }
	@RequestMapping(value = {"/updatedone"})
    public ModelAndView updatedone(HttpServletRequest request,HttpSession session) {
    	ModelAndView model = new ModelAndView("setmovie");
    	String name=request.getParameter("mov_name");
    	String dir=request.getParameter("mov_dir");
    	String date=request.getParameter("mov_date");
    	int released=Integer.parseInt(request.getParameter("released"));
    	dao.movieupdate(name, dir, date, released,(int)session.getAttribute("movieid") );
    	List<Movie> mov=dao.findall();
      	 model.addObject("movlist", mov);
       return model;
    }
	@RequestMapping(value = {"/createmovie"})
    public ModelAndView createmovie() {
    	ModelAndView model = new ModelAndView("createmovie");
       return model;
    }
	@RequestMapping(value = {"/createdone"})
    public ModelAndView createdone(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("setmovie");
    	String name=request.getParameter("mov_name");
    	String dir=request.getParameter("mov_dir");
    	String date=request.getParameter("mov_date");
    	int released=Integer.parseInt(request.getParameter("released"));
    	int maxmovieid=dao.findmaxid()+1;
    	dao.createmovie(name, dir, date, released, maxmovieid);
    	List<Movie> mov=dao.findall();
     	 model.addObject("movlist", mov);
       return model;
	}
	@RequestMapping(value = {"/scrcreatedone"})
    public ModelAndView scrcreatedone(HttpServletRequest request, HttpSession session) {
		ModelAndView model = new ModelAndView("setscreening");
    	String room=request.getParameter("room");
    	String time=request.getParameter("scr_time");
    	int movid=(int)session.getAttribute("movieid");
    	int maxscrid=dao.findmaxscrid()+1;
    	dao.createscr(time, room, maxscrid, movid);
    	List<Movie> mov=dao.findall();
     	 model.addObject("movlist", mov);
     	 
     	 if(room.equalsIgnoreCase("a")){
     		 for(int i=1;i<=6;i++)
     		 dao.createseat(maxscrid, i);
     	 }else{for(int i=1;i<=4;i++)
     		 dao.createseat(maxscrid, i);}
     	List<screening> scrlist=dao.findallscreening();
    	model.addObject("scrlist",scrlist);
       return model;
	}
	@RequestMapping(value = {"/moviesearch"})
	 public ModelAndView moviebysearch(@ModelAttribute ("searchmovie") String searchmovie) throws SQLException{
    	ModelAndView model = new ModelAndView("moviebysearch");
    	 List<Movie> mov=dao.findbysearch(searchmovie);
    	 model.addObject("moviebysearch",mov);
       return model;
	}
	@RequestMapping(value = {"/theaterintro"})
    public ModelAndView theaterintro() {
    	ModelAndView model = new ModelAndView("theaterintro");
       return model;
    }
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                // 这里只是简单例子，文件直接输出到项目路径下。
                // 实际项目中，文件需要输出到指定位置，需要在增加代码处理。
                // 还有关于文件格式限制、文件大小限制，详见：中配置。
                BufferedOutputStream out = new BufferedOutputStream(
                        new FileOutputStream(new File(file.getOriginalFilename())));
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "上传失败," + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "上传失败," + e.getMessage();
            }
            return "上传成功";
        } else {
            return "上传失败，因为文件是空的.";
        }
    }

	
	
}
