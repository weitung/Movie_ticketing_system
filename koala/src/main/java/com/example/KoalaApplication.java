package com.example;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KoalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KoalaApplication.class, args);
	}
	
}
