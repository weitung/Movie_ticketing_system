package com.example.DAO;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.example.entity.Member;

@Repository
public  class memberDAO {
	@Autowired
	 private DataSource dataSource;
	 @Autowired
	 JdbcTemplate jdbcTemplate;
	 public int insert(Member mem) {
		  return jdbcTemplate.update(
		    "insert into cinema.member (id,name,account,email,cellphone,birthday,sex) values(?,?,?,?,?,?,?)",
		    mem.getId(),mem.getName(),mem.getAccount(),mem.getEmail(),mem.getCellphone(),mem.getBirthday(),mem.getSex());
		 }
	 public int findid(){
		 String sql="SELECT max(id) FROM cinema.member;";
		 return this.jdbcTemplate.queryForObject(sql,Integer.class);
}
}