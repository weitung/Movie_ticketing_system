package com.example.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javassist.bytecode.Descriptor.Iterator;



import javax.sql.DataSource;
import javax.swing.tree.TreePath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import org.springframework.stereotype.Repository;

import com.example.entity.Movie;
import com.example.entity.screening;
import com.example.entity.seat;

@Repository
public class MovieDAO{
	 
	@Autowired
	 private DataSource dataSource;
	 @Autowired
	 JdbcTemplate jdbcTemplate;

	public List<Movie> findall(){
		return this.jdbcTemplate.query( "select * from cinema.movielist", new movieMapper());

	}
	public List<Movie> findmoviebymovieid(int id){
		String sql="SELECT * FROM cinema.movielist where movie_id="+id;
		return this.jdbcTemplate.query( sql, new movieMapper());

	}
	public List<Movie> findbysearch(String input){
		return this.jdbcTemplate.query( "select * from cinema.movielist where movie_name like '%"+input+"%'", new movieMapper());

	}
	 private static final class movieMapper implements RowMapper<Movie> {

	     public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
	         Movie mov = new Movie();
	         mov.setId(rs.getInt("movie_id"));
	         mov.setName(rs.getString("movie_name"));
	         mov.setDirector(rs.getString("movie_director"));
	         mov.setReleasedate(rs.getString("movie_releasedate"));
	         mov.setReleased(rs.getInt("movie_released"));
	         return mov;
	     }
		
	 }
	 public List<screening> findallscreening(){
			String sql="SELECT movielist.movie_id,screening.screening_datetime,movielist.movie_name,screening.screening_id ,screening_id,screening.room FROM cinema.movielist"
					+ " inner join cinema.screening on movielist.movie_id = screening.movie_id";
			return this.jdbcTemplate.query( sql, new scrMapper());
		}
	 public List<screening> findscreeningbymovieid(int id){
			String sql="SELECT movielist.movie_id,screening.screening_datetime,movielist.movie_name,screening.screening_id ,screening_id,screening.room FROM cinema.movielist"
					+ " inner join cinema.screening on movielist.movie_id = screening.movie_id where movielist.movie_id="+id;
			return this.jdbcTemplate.query( sql, new scrMapper());

		}
	 public List<screening> findscreeningbyscreningid(int id){
			String sql="SELECT movielist.movie_id,screening.screening_datetime,movielist.movie_name,screening.screening_id,screening.room FROM cinema.movielist"
					+ " inner join cinema.screening on movielist.movie_id = screening.movie_id where screening.screening_id="+id;
			return this.jdbcTemplate.query( sql, new scrMapper());

		}
	 private static final class scrMapper implements RowMapper<screening> {

	 public screening mapRow(ResultSet rs, int rowNum) throws SQLException {
         screening scr = new screening();
         
         scr.setMovie_id(rs.getInt("movie_id"));
         scr.setTime(rs.getString("screening_datetime"));
         scr.setName(rs.getString("movie_name"));
         scr.setScreening_id(rs.getInt("screening_id"));
         scr.setRoom(rs.getString("room"));
         return scr;
     }}
	 public List<seat> findseatbyscreningid(int id){
		 String sql="SELECT * FROM cinema.seat where screening_id="+id+" and solded=0;";
		 return this.jdbcTemplate.query( sql, new seatMapper());
	 }
	 private static final class seatMapper implements RowMapper<seat> {

		 public seat mapRow(ResultSet rs, int rowNum) throws SQLException {
	         seat seat = new seat();
	         seat.setScreening_id(rs.getInt("screening_id"));
	         seat.setSeat_id(rs.getInt("seat_id"));
	         seat.setSolded(rs.getInt("solded"));
	         return seat;
	     }}
	 public void seatupdate(int seatid,int screeningid) {
		 
		   jdbcTemplate.update("UPDATE cinema.seat SET solded=1 WHERE screening_id="+screeningid+" and seat_id="+seatid);
		}
	 public String findroombyscreningid(int id){
		 String sql="SELECT room FROM cinema.screening where screening_id="+id;
		 return this.jdbcTemplate.queryForObject(sql, String.class);
	 }
	 public int findseatquqntitybyscreningid(int id){
		 String sql="SELECT count(screening_id) FROM cinema.seat where screening_id="+id+" and solded=0;";
		 return this.jdbcTemplate.queryForObject(sql, Integer.class);
	 }
	 public void movieupdate(String name,String director,String releasedate,int released,int movieid) {
		 String sql="UPDATE `cinema`.`movielist` SET `movie_name`='"+name+"', `movie_director`='"+director+"', `movie_releasedate`='"+releasedate+"', `movie_released`='"+released+"' WHERE `movie_id`='"+movieid+"';";
		   jdbcTemplate.update(sql);
		}
	 public int findmaxid(){
		 String sql="SELECT max(movie_id) FROM cinema.movielist;";
		 return this.jdbcTemplate.queryForObject(sql, Integer.class);
	 }
	 public void createmovie(String name,String director,String releasedate,int released,int movieid) {
		 String sql="INSERT INTO `cinema`.`movielist` (`movie_id`, `movie_name`, `movie_director`, `movie_releasedate`, `movie_released`) VALUES ('"+movieid+"', '"+name+"', '"+director+"', '"+releasedate+"', '"+released+"');";
		 jdbcTemplate.execute(sql);
	 }
	 public void createseat(int screeningid,int seatid) {
		 String sql="INSERT INTO `cinema`.`seat` (`screening_id`, `seat_id`, `solded`) VALUES ('"+screeningid+"', '"+seatid+"', '0');";
		 jdbcTemplate.execute(sql);
	 }
	 public void deletemovie(int movieid) {
		 String sql="DELETE FROM cinema.movielist WHERE movie_id="+movieid;
		 jdbcTemplate.execute(sql);
	 }
	 public int findmaxscrid(){
		 String sql="SELECT max(screening_id) FROM cinema.screening;";
		 return this.jdbcTemplate.queryForObject(sql, Integer.class);
	 }
	 public void createscr(String time,String room,int scrid,int movieid) {
		 String sql="INSERT INTO `cinema`.`screening` (`screening_id`, `movie_id`, `screening_datetime`, `room`) VALUES ('"+scrid+"', '"+movieid+"', '"+time+"', '"+room+"');";
		 jdbcTemplate.execute(sql);
	 }
}