package com.example.entity;

public class screening {
private int movie_id;
private String time;
private String name;
private int screening_id;
private String room;

public screening() {
	super();
}
public screening(int movie_id, String time , String name,int screening_id,String room) {
	super();
	this.movie_id = movie_id;
	this.time = time;
	this.name=name;
	this.screening_id=screening_id;
	this.room=room;
}
public int getMovie_id() {
	return movie_id;
}
public void setMovie_id(int movie_id) {
	this.movie_id = movie_id;
}
public String getTime() {
	return time;
}
public void setTime(String time) {
	this.time = time;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getScreening_id() {
	return screening_id;
}
public void setScreening_id(int screening_id) {
	this.screening_id = screening_id;
}
public String getRoom() {
	return room;
}
public void setRoom(String room) {
	this.room = room;
}

}
