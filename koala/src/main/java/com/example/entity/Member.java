package com.example.entity;

public class Member {

private int id;
private String name;
private String account;
private String email;
private int cellphone;
private int birthday;
private String sex;

public Member() {
	super();
}
public  Member(int id, String name, String account, String email,int cellphone,int birthday,String sex) {
	this.id = id;
	this.name = name;
	this.account = account;
	this.email = email;
	this.cellphone = cellphone;
	this.birthday = birthday;
	this.sex = sex;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAccount() {
	return account;
}
public void setAccount(String account) {
	this.account = account;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public int getCellphone() {
	return cellphone;
}
public void setCellphone(int cellphone) {
	this.cellphone = cellphone;
}
public int getBirthday() {
	return birthday;
}
public void setBirthday(int birthday) {
	this.birthday = birthday;
}
public String getSex() {
	return sex;
}
public void setSex(String sex) {
	this.sex = sex;
}

}