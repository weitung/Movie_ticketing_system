package com.example.entity;

public class seat {
int screening_id;
int seat_id;
int solded;

public seat() {
	super();
}
public seat(int screening_id, int seat_id, int solded) {
	super();
	this.screening_id = screening_id;
	this.seat_id = seat_id;
	this.solded = solded;
}
public int getScreening_id() {
	return screening_id;
}
public void setScreening_id(int screening_id) {
	this.screening_id = screening_id;
}
public int getSeat_id() {
	return seat_id;
}
public void setSeat_id(int seat_id) {
	this.seat_id = seat_id;
}
public int getSolded() {
	return solded;
}
public void setSolded(int solded) {
	this.solded = solded;
}

}
