package com.example.entity;



public class Movie {
 private int id;
 private String name;
 private String director;
 private String releasedate;
 private int released;
 private String rele;
 public Movie(){rele=setrele(this.released);};
public Movie(int id, String name, String director, String releasedate) {
	this.id = id;
	this.name = name;
	this.director = director;
	this.releasedate = releasedate;
	rele=setrele(this.released);
}

public int getReleased() {
	return released;
}
public void setReleased(int released) {
	this.released = released;
	rele=setrele(this.released);
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public String getReleasedate() {
	return releasedate;
}
public void setReleasedate(String releasedate) {
	this.releasedate = releasedate;
}
public String setrele(int r){
	if(r==1){return "是";}
	else{return "否";}
	
}
public String getRele() {
	return rele;
}
public void setRele(String rele) {
	this.rele = rele;
}

}
